## TOC

1. [Token](#markdown-header-token)
2. [Return code](#markdown-header-return-code)
3. [User Register](#markdown-header-user-register)
4. [User Login](#markdown-header-user-login)
5. [User Logout](#markdown-header-user-logout)
6. [Token Refeshing](#markdown-header-token-refeshing)
7. [Send OTP](#markdown-header-send-otp)
8. [QR Code](#markdown-header-qr-code)
9. [Sending Feedback](#markdown-header-sending-feedback)
10. [Sending Request](#markdown-header-sending-request)
11. [Fetching News Feed](#markdown-header-fetching-news-feed)
12. [Fetching Point History](#markdown-header-fetching-point-history)
13. [User Profile](#markdown-header-user-profile)
14. [Update User Profile](#markdown-header-update-user-profile)
15. [Notification](#markdown-header-notification)
16. [Notification - Mark as read](#markdown-header-notification-mark-as-read)
17. [Testing](#markdown-header-testing)

## Token
[^top](#markdown-header-toc)

HEADER: `Authorization: Bearer HASH`
or
Request parameter: `token: HASH`

## Return code
[^top](#markdown-header-toc)

- 1: Success
- Other: Error, with message.

## User Register
[^top](#markdown-header-toc)

`POST http://test.tugo.com.vn/api/http.php/v1/app/register`

Sample request:

```
{
    "phone_number": "01692998182",
    "device_id": "abc123",
    "customer_name": "Bửu Phạm"
}
```

Sample response:

```
{
    "status": "OK",
    "code": 1
}
```
Return code:

- 1001: Chưa nhập số điện thoại
- 1002: Chưa có mã thiết bị
- 1003: Chưa nhập tên khách hàng
- 1005: Đăng ký chưa thành công, vui lòng thử lại
- 1525: Khách đã đăng ký thông tin (user exists)
- 1999: Lỗi khác - có thông báo riêng

---
## User Login
[^top](#markdown-header-toc)

`POST http://test.tugo.com.vn/api/http.php/v1/app/login`

Sample request data:

```
{
    "phone_number": "01692998182",
    "device_id": "abc123",
    "cloud_message_token": "1234567890abcdef",
    "otp": "1234"
}
```

Sample response data:

```
{
    "status": "OK",
    "code": 1,
    "data": {
        "user": {
            "uuid": "9956a3dc21e5c5f8",
            "customer_number": "975386424",
            "customer_name": "Bửu Phạm",
            "phone_number": "01692998182",
            "register_at": "2017-08-06 22:54:38",
            "last_active": null,
            "user_point": "0"
        },
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MDIxMjYwMzAsImlzcyI6IlR1Z28iLCJuYmYiOjE1MDIwMzk1NzAsImlhdCI6MTUwMjAzOTYzMCwiY3VzdG9tZXJfbmFtZSI6IkJcdTFlZWR1IFBoXHUxZWExbSIsInBob25lX251bWJlciI6IjAxNjkyOTk4MTgyIiwidXVpZCI6Ijk5NTZhM2RjMjFlNWM1ZjgifQ.15swFTLcgT65c2XohjBq-eihlMssNaWcmLShAZ_NcJE",
        "refresh_token": "cf0fc291bacb49f7cf4aee7cf96ac1aa"
    }
}
```

Return code:

- 1001: Chưa nhập số điện thoại
- 1002: Chưa có mã thiết bị
- 1004: Không tìm thấy thông tin khách, vui lòng thử lại
- 1008: Lỗi tạo mã token
- 1009: Lỗi tạo mã refresh token
- 1017: Chưa có mã OTP
- 1523: OTP không đúng

---
## User Logout
[^top](#markdown-header-toc)

`POST http://test.tugo.com.vn/api/http.php/v1/app/logout`

**Token required**

Sample request data:

```
{
    "device_id": "abc123"
}
```

Sample response data:

```
{
    "status": "Success",
    "code": 1
}
```

Return code:

- 1010: Chưa có thông tin khách
- 1002: Chưa có mã thiết bị
- 1004: Không tìm thấy thông tin khách, vui lòng thử lại

---
## Token Refeshing
[^top](#markdown-header-toc)

`POST http://test.tugo.com.vn/api/http.php/v1/app/refresh`

Sample request data:

```
{
	"phone_number": "01692998182",
	"device_id": "abc123",
	"refresh_token": "31f06f10a482655196633c5fbf55c196"
}
```

OR

```
{
	"uuid": "abc123456abc",
	"device_id": "abc123",
	"refresh_token": "31f06f10a482655196633c5fbf55c196"
}
```

Sample response data:

```
{
    "status": "Success",
    "code": 1,
    "data": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MDIxODI4ODQsImlzcyI6IlR1Z28iLCJuYmYiOjE1MDIwOTY0MjQsImlhdCI6MTUwMjA5NjQ4NCwiY3VzdG9tZXJfbmFtZSI6IkJcdTFlZWR1IFBoXHUxZWExbSIsInBob25lX251bWJlciI6IjAxNjkyOTk4MTgyIiwidXVpZCI6Ijk5NTZhM2RjMjFlNWM1ZjgifQ.5WCUwevgxZbN6e_i_SS8452IedsqwmNWcAnhV9372Z4"
    }
}
```

Return code:

- 1007: Chưa có mã refresh_token
- 1010: Chưa có thông tin khách hàng
- 1002: Chưa có mã thiết bị
- 1008: Lỗi tạo mã token

## Send OTP
[^top](#markdown-header-toc)

`POST http://test.tugo.com.vn/api/http.php/v1/app/otp`

Sample request data:

```
{
	"phone_number": "01692998182",
	"device_id" : "abc123",
	"checksum": "HASH_________________________________"
}
```

Sample response data:

```
{
    "status": "Success",
    "code": 1
}
```

```
{
    "status": "Error",
    "message": "Số đang spam",
    "code": 1702
}
```

Return code:

- 1001: Chưa có số điện thoại
- 1002: Chưa có mã thiết bị
- 1015: Chưa có mã checksum
- 1016: Dữ liệu không hợp lệ
- 1014: Lỗi gửi OTP
- 1702: Số đang spam - bị chặn, không gửi OTP
- 1004: Không tìm thấy thông tin khách, vui lòng thử lại

## QR Code
[^top](#markdown-header-toc)

`GET http://test.tugo.com.vn/api/http.php/v1/app/qrcode`

**Token required**

Sample response data:

```
{
    "status": "Success",
    "code": 1,
    "data": {
        "qrcode": "http://test.tugo.com.vn/public_file/qrcode/bb2b2e437c3af70bee0bbc06e331dac0.png"
    }
}
```

Return code:

- 1010: Chưa có thông tin khách
- 1011: Không tạo được QR Code

## Sending Feedback
[^top](#markdown-header-toc)

`POST http://test.tugo.com.vn/api/http.php/v1/app/feedback`

**Token required**

Sample request data:

```
{
	"email": "buu@tugo.com.vn",
	"request_content": "abc test aaaa",
}
```

Sample response data:

```
{
    "status":"Success",
    "code":1
}
```

Return code:

- 1010: Chưa có thông tin khách
- 1012: Chưa có email
- 1013: Chưa có nội dung

## Sending Request
[^top](#markdown-header-toc)

`POST http://test.tugo.com.vn/api/http.php/v1/app/request`

**Token required**

Sample request data:

```
{
	"email": "buu@tugo.com.vn",
	"request_content": "abc test aaaa",
}
```

Sample response data:

```
{
    "status":"Success",
    "code":1
}
```

Return code:

- 1010: Chưa có thông tin khách
- 1012: Chưa có email
- 1013: Chưa có nội dung

## Fetching News Feed
[^top](#markdown-header-toc)

`GET http://test.tugo.com.vn/api/http.php/v1/app/news`

**Token required**

Parameters:

- page: number (default: 1)

Sample response data:

```
{
    "status": "Success",
    "code": 1,
    "data": [
        {
            "title": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque"
        },
        {
            "title": "Lorem ipsum dolor sit amet,",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut"
        },
        {
            "title": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et"
        },
        {
            "title": "Lorem ipsum dolor sit amet, consectetuer adipiscing",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed,"
        },
        {
            "title": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi"
        },
        {
            "title": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis."
        },
        {
            "title": "Lorem ipsum dolor sit amet,",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc"
        },
        {
            "title": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque"
        },
        {
            "title": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae,"
        },
        {
            "title": "Lorem ipsum dolor sit amet,",
            "content": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum."
        }
    ],
    "total": 100,
    "page": 2,
    "limit": 10
}
```

## Fetching Point History
[^top](#markdown-header-toc)

`GET http://test.tugo.com.vn/api/http.php/v1/app/point-history`

**Token required**

Parameters:

- page: number (default: 1)

Sample response data:

```
{
    "status": "Success",
    "code": 1,
    "data": [
        {
            "booking_code": 688709,
            "point": 33,
            "date": "2017-09-01 12:09:39",
            "amount": 4192612
        },
        {
            "booking_code": 946099,
            "point": 40,
            "date": "2017-02-07 00:54:11",
            "amount": 1260620
        },
        {
            "booking_code": 445236,
            "point": 64,
            "date": "2016-08-26 11:58:40",
            "amount": 494939
        },
        {
            "booking_code": 958372,
            "point": 39,
            "date": "2016-12-06 08:26:07",
            "amount": 997250
        },
        {
            "booking_code": 487298,
            "point": 88,
            "date": "2017-06-27 04:34:56",
            "amount": 3581121
        },
        {
            "booking_code": 655101,
            "point": 99,
            "date": "2016-01-06 06:48:06",
            "amount": 3291387
        },
        {
            "booking_code": 430452,
            "point": 66,
            "date": "2017-03-25 20:44:12",
            "amount": 3789342
        },
        {
            "booking_code": 199126,
            "point": 50,
            "date": "2017-06-08 19:44:04",
            "amount": 641260
        },
        {
            "booking_code": 138618,
            "point": 58,
            "date": "2017-04-22 19:40:29",
            "amount": 1047540
        },
        {
            "booking_code": 741938,
            "point": 95,
            "date": "2017-07-17 00:42:58",
            "amount": 495035
        }
    ],
    "total": 100,
    "page": 2,
    "limit": 10
}
```

## User Profile
[^top](#markdown-header-toc)

`GET http://test.tugo.com.vn/api/http.php/v1/app/me`

**Token required**

Parameters: none

Sample response data:

```
{
    "status": "Success",
    "code": 1,
    "data": {
        "uuid": "9956a3dc21e5c5f8",
        "customer_name": "xxx",
        "customer_number": "975386424",
        "phone_number": "01692998182",
        "user_point": "0",
        "qrcode": "http://test.tugo.com.vn/public_file/qrcode/file_name.png"
    }
}
```


## Update User Profile
[^top](#markdown-header-toc)

`POST http://test.tugo.com.vn/api/http.php/v1/app/me`

**Token required**

Sample request data:

```
{
	"customer_name": "New Name"
}
```

Sample response data:

```
{
    "status": "Success",
    "code": 1,
    "data": {
        "uuid": "9956a3dc21e5c5f8",
        "customer_name": "New Name",
        "customer_number": "975386424",
        "phone_number": "01692998182",
        "user_point": "0",
        "qrcode": "http://localhost:1180/public_file/qrcode/file_name.png"
    }
}
```

Return code:

- 1003: Chưa nhập tên khách hàng
- 1019: Cập nhật bị lỗi

## Notification
[^top](#markdown-header-toc)

Payload:

```
{
    "type": "TYPE", // định nghĩa bên dưới
    "url" : "URL" // optional
}
```

Types:

- `url`: open URL
- `point`: Điểm thưởng tăng/giảm -> Trang chủ/trang lịch sử điểm
- `promo`: Chương trình khuyến mãi -> Trang thông báo
- `tour`: Khi đặt tour, thanh toán, sắp đến ngày khởi hành... -> Trang thông báo

## Notification - Mark as read
[^top](#markdown-header-toc)

`POST http://test.tugo.com.vn/api/http.php/v1/app/notification`

**Token required**

Sample request data:

```
{
	"notification_id": "ID"
}
```

Sample response data:

```
{
    "status": "Success",
    "code": 1
}
```

## Testing
[^top](#markdown-header-toc)

Register with this phone number: `0000000009`.
Login with the following information:

- Phone Number: `0000000009`
- OTP: `5555`